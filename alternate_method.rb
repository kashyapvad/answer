class AlternateMethod  < String #usinh string as superclass so that methods on string can be used on our class

    def addnumbtoname # method to generate number at the end of the name
      res = self + " " + (6..15).to_a.shuffle[3].to_s # using self (inside the class self is the object itself) to get name and using shuffle and to_s method to add randomly generated number string and storing it in a variable
      puts res # using puts to display the string stored in res variable
    end
end
