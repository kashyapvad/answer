class GenNumForName
  attr_accessor :name # creates attribute accessor corresponding to a name
  def initialize(attributes = {}) #it’s the method called when we execute GenNumForName.new
    @name  = attributes[:name] #Storing the name into the instance variable @name
  end

  def addnumtoname #method to add number at the end of the name
    "#{@name}" + " " + (6..15).to_a.shuffle[3].to_s #interpolating the @name string with a randomly generated number using shuffle method and coversting the number into string using to_s so that it can be interpolated with @name
  end

end


